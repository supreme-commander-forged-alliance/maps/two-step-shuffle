version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Center of Tatsu",
    description = "A map based on the design of a community member. See also: https://forum.faforever.com/topic/1214/a-community-effort-on-a-map-layout",
    preview = '',
    map_version = 1,
    type = 'skirmish',
    starts = true,
    size = {512, 512},
    reclaim = {0, 0},
    map = '/maps/two_step_shuffle_core/two_step_shuffle.scmap',
    save = '/maps/two_step_shuffle_core/two_step_shuffle_save.lua',
    script = '/maps/two_step_shuffle_core/two_step_shuffle_script.lua',
    norushradius = 40,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2', 'ARMY_3', 'ARMY_4'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN' ),
            },
        },
    },
}
