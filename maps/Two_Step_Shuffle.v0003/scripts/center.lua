-- Made for Sanctuary
-- Made by (Jip) Willem Wijnia

local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local EffectUtilities = import('/lua/EffectUtilities.lua');
local EffectTemplates = import('/lua/EffectTemplates.lua');

local util = import('/lua/utilities.lua')

local Entity = import('/lua/sim/Entity.lua').Entity
local Unit = import('/lua/sim/Unit.lua').Unit

local function MakeImmune(units)
    for k, unit in units do 
        unit:SetReclaimable(false);
        unit:SetCapturable(false);
        unit:SetDoNotTarget(true);
        unit:SetRegenRate(5000);
        unit:SetUnSelectable(true);
        unit:SetCanTakeDamage(false);
        unit:SetCanBeKilled(false);
    end
end

local function CreateShields()

    local OnCollisionCheck = function(self, other)
        return true
    end

    local shields = ScenarioUtils.CreateArmyGroup("NEUTRAL_CIVILIAN", "Shields")
    for k, unit in shields do 
        -- change the shield radius
        unit.MyShield.RegenRate = 2000
        unit.MyShield.RegenStartTime = 0
        unit.MyShield.ShieldVerticalOffset = -133.5
        unit.MyShield.Size = 280

        -- change the collision check
        unit.MyShield.OnCollisionCheck = OnCollisionCheck
    end

    MakeImmune(shields)
    return shields
end

local function GiveEnergy(brain)

    brain:GiveStorage('Energy', 1000)

    ForkThread(
        function()
            while true do 
                WaitSeconds(0.1)
                brain:GiveResource('Energy', 50)
            end
        end
    )
end

local function ProvideVision(brains, position)
    for k, brain in brains do 
        ScenarioFramework.CreateVisibleAreaLocation(20, position, 0, brain)
    end
end

local function CreateEntities(ents, positionOffset, eulerOffset)

    positionOffset = positionOffset or Vector(0, 0, 0)
    eulerOffset = eulerOffset or Vector(0, 0, 0)

    for k, ent in ents do 
        local bp = __blueprints[ent.type]

        -- construct the entity
        local entity = Entity()
        entity:SetMesh(bp.Display.MeshBlueprint)
        entity:SetScale(bp.Display.UniformScale)

        -- position it
        local position = ent.Position 
        position = VAdd(position, positionOffset)
        entity:SetPosition(position, true)

        -- orientate it
        local euler = ent.Orientation
        euler = VAdd(euler, eulerOffset)
        local quat = ScenarioUtils.EulerToQuaternion(euler[1], euler[2], euler[3])
        entity:SetOrientation(quat, true)
    end
end

function CreateArtifacts(scenario)

    -- general place to look for the units
    local info = scenario.Env.Scenario.Armies.NEUTRAL_CIVILIAN.Units

    -- specific unit(s)
    local ents = info.Units.Other.Units
    local atln = info.Units.Atlantis.Units
    local shld = info.Units.Shields.Units
    local tl = info.Units.Left.Units
    local tr = info.Units.Right.Units
    local ts = info.Units.Support.Units

    -- variables that depend on the shield
    local center = false

    -- spawn the shield
    local shields = CreateShields()
    center = shields[1]:GetPosition()

    -- make sure the shield can be sustained
    GiveEnergy(GetArmyBrain("NEUTRAL_CIVILIAN"))
    ProvideVision(ArmyBrains, center)

    -- create the other units as entities
    CreateEntities(atln, Vector(0, 2.5, 0))
    CreateEntities(ents, Vector(0, 0, 0))
    CreateEntities(tl, Vector(0, 0, 0), Vector(0, -0.5 * 3.14, 0.5 * 3.14))
    CreateEntities(tr, Vector(0, 0, 0), Vector(0, 0.5 * 3.14, -0.5 * 3.14))
    CreateEntities(ts, Vector(0, 0, 0), Vector(0, 3.14, 0))

end