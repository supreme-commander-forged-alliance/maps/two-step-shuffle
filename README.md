## Two Step Shuffle

A map made based on a design by Tatsu as part of a community effort on map designs. For more information:
 - https://forum.faforever.com/topic/1214/a-community-effort-on-a-map-layout

![](images/preview-1.png)

## Statistics of the map

The map is designed to be played as a 2 vs 2. Both teams start in their own valley with only one circular connection to the rest of the map. It is a casual map with an aim at how to make it competitively easy to read while using the latest techniques.

There is reclaim on this map. This includes:
 - A lot of trees for energy.
 - A few rocks near for mass.

![](/images/preview-2.png)

## Technical aim of the map

This map was made over a weekend, costing in total about 10 hours. At the moment of writing it still requires a few tweaks and twigs before it is completed. The goal was to optimize the workflow between World Machine and the game. Via Image Magick I was able to exclude Photoshop from the pipeline completely - solely relying on the nodes of World Machine to the assets as intended and then used Image Magick to convert it to the correct format / size.

There is a shell script inside the core map that automates this conversion process completely. It is faster than Photoshop computation wise and it allows me to do a 'click-and-go' operation: I can click it, get some coffee, and view the results when I'm back discussing how the breakout rooms during lectures are a strange phenomenon. 

![](images/preview-3.png)

## Installation guide
The map is supported by any Supreme Commander instlalation. 

Clone or download this repository. If downloaded, unzip the download. Find the non-core version of Center of Tatsu inside the maps folder. Open a new window and navigate towards your _My Documents_ folder. From there, navigate to: 
``` sh
".../My Documents/My Games/Gas Powered Games/Supreme Commander: Forged Alliance/Maps"
```
If the _Maps_ folder does not exist, create one. Copy the non-core version to the maps folder in question.

## License

The stratum layers (/env/layers) are from www.textures.com. I am obligated to add this text:
_One or more textures bundled with this project have been created with images from Textures.com. These images may not be redistributed by default. Please visit www.textures.com for more information._

All other assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
