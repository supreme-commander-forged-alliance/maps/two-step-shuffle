-- Made for Sanctuary
-- Made by (Jip) Willem Wijnia

local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local EffectUtilities = import('/lua/EffectUtilities.lua');
local EffectTemplates = import('/lua/EffectTemplates.lua');

local util = import('/lua/utilities.lua')

local Entity = import('/lua/sim/Entity.lua').Entity
local Unit = import('/lua/sim/Unit.lua').Unit

local effects = {
    EffectTemplates.TreeBurning01,
    EffectTemplates.TreeBurning01,
    EffectTemplates.DamageStructureFireSmoke01
}

local function CreateEntities(ents, positionOffset, eulerOffset)

    positionOffset = positionOffset or Vector(0, 0, 0)
    eulerOffset = eulerOffset or Vector(0, 0, 0)

    for k, ent in ents do 
        local bp = __blueprints[ent.type]

        -- construct the entity
        local entity = Entity()
        entity:SetMesh(bp.Display.MeshBlueprint)
        entity:SetScale(bp.Display.UniformScale)

        -- position it
        local position = ent.Position 
        position = VAdd(position, positionOffset)
        entity:SetPosition(position, true)

        -- orientate it
        local euler = ent.Orientation
        euler = VAdd(euler, eulerOffset)
        local quat = ScenarioUtils.EulerToQuaternion(euler[1], euler[2], euler[3])
        entity:SetOrientation(quat, true)

        -- add emitters to czar
        for k = 1, 10 do 
            local indexBone = math.floor(Random() * entity:GetBoneCount())
            local bone = entity:GetBoneName(indexBone)

            local indexEmitter = math.floor(Random() * table.getn(effects)) + 1
            local emitters = EffectUtilities.CreateBoneEffects(entity, bone, "NEUTRAL_CIVILIAN", effects[indexEmitter])
        end

        local rect = ScenarioUtils.AreaToRect("Crash")
        local props = GetReclaimablesInRect(rect)

        -- add emitters to random trees
        for k = 1, 10 do 
            local indexProp = math.floor(Random() * table.getn(props)) + 1
            local prop = props[indexProp]

            local indexBone = math.floor(Random() * entity:GetBoneCount())
            local bone = entity:GetBoneName(indexBone)

            local indexEmitter = math.floor(Random() * table.getn(effects)) + 1
            local emitters = EffectUtilities.CreateEffects(prop, "NEUTRAL_CIVILIAN", effects[indexEmitter])
            
            for k, emitter in emitters do 
                prop.Trash:Add(emitter)
            end
        end
    end
end

function CreateArtifacts(scenario)
    -- general place to look for the units
    local info = scenario.Env.Scenario.Armies.NEUTRAL_CIVILIAN.Units

    -- specific unit(s)
    local ents = info.Units.CZAR.Units

    -- create the other units as entities
    CreateEntities(ents, Vector(0, 8, 0))
end