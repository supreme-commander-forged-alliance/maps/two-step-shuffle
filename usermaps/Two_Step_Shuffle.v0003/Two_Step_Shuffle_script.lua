-- Made for Sanctuary
-- Made by (Jip) Willem Wijnia

local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

function OnPopulate()
    ScenarioUtils.InitializeArmies()
    ScenarioFramework.SetPlayableArea('AREA_1' , false)
end

function OnStart(scenario)

    -- prevent them from sharing, army_2 is apparently allied with them
    local neutrals = GetArmyBrain("NEUTRAL_CIVILIAN");
    neutrals:SetResourceSharing(false);

    import("/maps/two_step_shuffle.v0003/scripts/center.lua").CreateArtifacts(scenario)
    import("/maps/two_step_shuffle.v0003/scripts/czar.lua").CreateArtifacts(scenario)
end
